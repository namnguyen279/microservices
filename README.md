# Start Application

- ApiCore được sử dụng làm light logic

Start application như dưới đây

```javascript
import { Application } from '@vcsc/api-core';

class App extends Application {
  constructor() {
    super();

    const defaultConfig = {
      port: 2345,
      name: 'Test-App',
      autoloadPaths: ['plugins', 'services'],
    };

    this.start(defaultConfig);
  }
}

new App();
```

# Installation

```
npm install @vcsc/api-core
```

# Autoload

- By default, app sẽ scan các folders `/plugins` và `/services` để load các files `.ts` và `.js`

- Để tự động load thêm folders, bạn có thể cho thêm vào array trong `autoloadPaths` như ví dụ Start Application (Note: Ví trí load từ trái sang phải)

- Các files phải `export default` để autoload

- Class phải extends class `Service`, `Route`, `Plugin`

- Class phải impliment method `install`, method sẽ run khi application start

## Custom Service | Custom Route | Custom Plugin

1. Route & Plugin are abstract of Service
2. Custom

```javascript

import { Service, Route, Plugin, Log } from '@vcsc/api-core';

export default class UserService extends Service {
  private static readonly instance: UserService = new UserService();

  public static get Instance(): UserService {
    return UserService.instance;
  }

  public install() {}
}

// Route / will return "to user"
export default class UserRoute extends Route {
  public install() {
    this.GET("/", this.reponse, this.format)
  }

  response (requestProps) {
    Log.print(requestProps)
    return "to"
  }

  format(result, requestProps) {
    return result + " user"
  }
}

export default class UserPlugin extends Plugin {
  public install() {}
}
```

# Project Structures & Good Practices

1. Export default Class
2. ClassName.route.ts => Custom Restful Route
3. ClassName.service.ts => Custom Service (Business Logic)
