import React, { ReactNode } from 'react';
import style from './button.module.scss';

export type ButtonProps = {
  /**
   * a node to be rendered in the special component.
   */
  children?: ReactNode;
};

export function Button({ children }: ButtonProps) {
  return <div className={style.button}>{children}</div>;
}
