window.__routes__ = [
  {
    regexPath: '/',
    componentName: 'Home',
  },
  {
    regexPath: '(.*)',
    componentName: 'Page404',
  },
];
