export const queues: Promise<unknown>[] = [];
export let results: Item[];
let storages = [] as Item[];

type Item = {
  name: string;
  value: unknown;
  resolver: unknown | Promise<unknown>;
};

if (typeof window !== null && (window as any).__data__) {
  storages = (window as any).__data__;
}

export async function preLoad(key: string, fn: Function | Promise<unknown>) {
  const existingKey = get(key);

  if (existingKey != undefined) {
    return existingKey;
  }

  const cell = Object.assign({ name: key }) as Item;

  if (typeof fn == 'function') {
    cell['resolver'] = fn();
  } else {
    cell['resolver'] = fn as Promise<Function>;
  }

  storages.push(cell);

  return storages[storages.length - 1];
}

export async function onLoad() {
  const resolvers = storages.map((item) => item.resolver);
  const results = await Promise.all(resolvers);

  results.forEach((item, index) => {
    storages[index].value = item;
  });

  if (typeof window !== null) {
    (window as any).__data__ = storages;
  }

  return storages;
}

export function get(key: string): Item | undefined {
  return storages.find((item) => item.name == key);
}
