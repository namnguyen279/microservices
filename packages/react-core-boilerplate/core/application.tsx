import React from 'react';
import { render, hydrate } from 'react-dom';

import { onLoad } from './preloader';
import { ReactElement } from 'react';

import { pathToRegexp, match } from 'path-to-regexp';

export interface Route {
  regexPath: string;
  componentName: string;
}

export interface Options {
  routes?: Route[];
  pages?: Function[];
}

let appRoute!: Route | undefined;
let appRoutes: Route[] = [];
const pathName = window?.location?.pathname;

function getRoute(routes: Route[]) {
  if (appRoute) {
    return appRoute;
  }

  appRoute = routes.find((route) => {
    const regexp = pathToRegexp(route.regexPath);
    return regexp.exec(window.location.pathname) != null;
  });

  return appRoute;
}

export function useRouteParser({ routes = appRoutes, pages = [] }: Options) {
  let route!: Route | undefined;

  if (routes) {
    route = getRoute(routes);
  }

  if (!route || routes.length == 0) {
    console.error('No Route is found');
  }

  const Page = pages.find((page) => page.name == route?.componentName);

  if (!Page || route == null) {
    return function () {
      return <></>;
    };
  }

  return function () {
    const path = route?.regexPath || '';
    const fn = match(path, { decode: decodeURIComponent });
    return <Page route={route} props={fn(pathName)} />;
  };
}

export abstract class Page {
  abstract App({ route }: any): ReactElement;
  pages: Function[];

  constructor(options?: Options) {
    this.pages = (options?.pages as Function[]) || [];
    appRoutes = options?.routes || appRoutes;
    this.Root = this.Root.bind(this);
  }

  public get(regexPath: string, componentName: string | Function) {
    if (typeof componentName == 'function') {
      appRoutes.push({ regexPath, componentName: componentName.name });
      return;
    }

    appRoutes.push({ regexPath, componentName });
  }

  public getRootElement() {
    let root = document.getElementById('root');

    if (!root) {
      root = document.createElement('div');
      root.id = 'root';
      document.body.appendChild(root);
    }

    return root;
  }

  public Root({ Component }: any) {
    try {
      const route = getRoute(appRoutes);

      const Page = this.pages.length > 0 ? useRouteParser({ pages: this.pages }) : null;

      return <Component route={route} Page={Page} />;
    } catch (e) {
      console.error(e);
    }

    return <></>;
  }

  public async start() {
    await onLoad();

    if (typeof window != 'undefined') {
      return this.client();
    }

    //TODO
    return this.server();
  }

  private client() {
    const root = this.getRootElement();

    const Root = this.Root;

    if (document.body.innerText !== '') {
      hydrate(<Root Component={this.App} />, root);
      return;
    }

    render(<Root Component={this.App} />, root);
  }

  //TODO
  private async server() {}
}

export function Router(options: Options) {}
