import React, { createContext } from 'react';

const memory = {};

export const AppContext = createContext(memory);
