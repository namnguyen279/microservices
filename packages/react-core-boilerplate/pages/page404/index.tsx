import React, { useEffect, useRef, useState } from 'react';
import { useLayoutEffect } from 'react';

export default function Page404({ route }: any) {
  // Redirection
  useLayoutEffect(() => {
    if (route.redirect) {
      const { to, delay } = route.redirect;

      const timer = setTimeout(() => {
        window.location.pathname = to;
      }, delay);

      return () => clearTimeout(timer);
    }
  }, []);
  return <>404</>;
}
