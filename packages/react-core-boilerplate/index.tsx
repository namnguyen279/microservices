import React from 'react';
import './root.scss';

import Application, { Options, useRouteParser } from 'core';

import Home from 'pages/home';

import Page404 from 'pages/page404';

import Nav from 'components/nav';

import { default as Layout, Sidebar, Body } from 'layout/window';

//Development
require('./_seed');

const options: Options = {
  pages: [Home, Page404],
  routes: (window as any).__routes__,
};

class Root extends Application {
  constructor() {
    super(options);
    this.start();
  }

  App({ route, Page }: any) {
    const Route = useRouteParser(options);

    return (
      <>
        <Layout />
        <Body>
          <Route />
        </Body>
        <Body>
          <div>dsadsadsa</div>
        </Body>
        <Sidebar>
          <Nav />
        </Sidebar>
      </>
    );
  }
}

new Root();
