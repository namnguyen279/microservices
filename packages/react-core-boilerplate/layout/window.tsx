import React, { createRef, useMemo, useLayoutEffect, LegacyRef } from 'react';
import { createPortal } from 'react-dom';
import cl from 'classnames';

import st from './window.module.scss';

let modal = createRef() as LegacyRef<HTMLDivElement>;
let header = createRef() as LegacyRef<HTMLDivElement>;
let sidebar = createRef() as LegacyRef<HTMLDivElement>;
let body = createRef() as LegacyRef<HTMLDivElement>;
let footer = createRef() as LegacyRef<HTMLDivElement>;

export default function Layout() {
  return (
    <div id="layout" className={st.layout}>
      <header id="header" className={st.header} ref={header}></header>
      <div className="content">
        <aside id="sidebar" className={st.sidebar} ref={sidebar}></aside>
        <div id="body" className={st.body} ref={body}></div>
      </div>
      <footer id="footer" className={st.footer} ref={footer}></footer>
      <div id="modal" className={st.modal} ref={modal}></div>
    </div>
  );
}

export function Portal({ append, children, locRef }: any) {
  const el = useMemo(() => document.createElement('div'), []);

  useLayoutEffect(() => {
    if (!locRef.current) return;

    if (!append) {
      locRef.current.innerHTML = '';
    }

    locRef.current.appendChild(el);

    return () => {
      locRef.current.removeChild(el);
    };
  });

  return createPortal(children, el);
}

export const Body = ({ children, append = false }: any) => {
  return <Portal append={append} locRef={body} children={children} />;
};

export const Sidebar = ({ children, append = false }: any) => {
  return <Portal append={append} locRef={sidebar} children={children} />;
};

export const Header = ({ children, append = false }: any) => {
  return <Portal append={append} locRef={header} children={children} />;
};

export const Modal = ({ children, append = false }: any) => {
  return <Portal append={append} locRef={modal} children={children} />;
};

export const Footer = ({ children, append = false }: any) => {
  return <Portal append={append} locRef={footer} children={children} />;
};
