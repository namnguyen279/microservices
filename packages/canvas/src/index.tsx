import React from 'react';
import './root.scss';

// import Home from 'structure/pages/canvas/home';
import Nav from 'structure/section/nav-sidebar';
import HeaderInfo from 'structure/section/header';
import FooterInfo from 'structure/section/footer';

import Application, { Options, useRouteParser } from 'core';

import { default as Layout, Sidebar, Body, Header, Footer } from 'layout/window';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro'; // <-- import styles to be used

(window as any).__routes__ = [
  {
    regexPath: '/',
    componentName: 'Home',
  },
  {
    regexPath: '(.*)',
    componentName: 'Page404',
  },
];

function Home() {
  return 'Home';
}

const options: Options = {
  pages: [Home],
  routes: (window as any).__routes__,
};

class Root extends Application {
  constructor() {
    super(options);
    this.start();
  }

  App({ route, Page }: any) {
    const Route = useRouteParser(options);

    return (
      <>
        <Layout />
        <Sidebar>
          <Nav />
          <FontAwesomeIcon icon={solid('user-secret')} />
          <FontAwesomeIcon icon={regular('coffee')} />
          <FontAwesomeIcon icon={brands('twitter')} />
        </Sidebar>
        <Body>
          <Header>
            <HeaderInfo />
          </Header>
          <Route />
          <Footer>
            <FooterInfo />
          </Footer>
        </Body>
      </>
    );
  }
}

new Root();
