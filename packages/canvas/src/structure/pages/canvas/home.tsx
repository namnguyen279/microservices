import React, { RefObject, useEffect, useRef, useState } from 'react';

import { Route } from 'core';
import { createRef } from 'react';
import s from './home.module.scss';
import { useLayoutEffect } from 'react';

const canvasRef = createRef() as RefObject<HTMLCanvasElement>;

class Canvas {
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;
  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.context = canvas.getContext('2d') as CanvasRenderingContext2D;
  }

  scale(scaleFactor = window.devicePixelRatio || 1) {
    const canvas = this.canvas as HTMLCanvasElement;

    canvas.style.width = canvas.style.width || canvas.width + 'px';
    canvas.style.height = canvas.style.height || canvas.height + 'px';
    canvas.width = Math.ceil(canvas.width * scaleFactor);
    canvas.height = Math.ceil(canvas.height * scaleFactor);

    this.context.scale(scaleFactor, scaleFactor);
  }

  draw(fn: Function) {
    fn(this.context);
  }
}

export default function Home({ route, props }: { route: Route; props: any }) {
  const ref = useRef() as RefObject<HTMLDivElement>;

  const nodesData = {
    '1234': {
      name: 'abc',
      x: 100,
      y: 100,
    },
    '1232': {
      name: 'abc',
      x: 400,
      y: 500,
    },
  };

  useLayoutEffect(function () {
    const canvas = new Canvas(canvasRef.current as HTMLCanvasElement);

    canvas.scale();

    canvas.draw((context: CanvasRenderingContext2D) => {
      for (var x = 0.5; x < window.innerHeight * 2; x += 10) {
        context.moveTo(x, 0);
        context.lineTo(x, window.innerHeight);
      }

      for (var y = 0.5; y < window.innerWidth * 2; y += 10) {
        context.moveTo(0, y);
        context.lineTo(window.innerWidth, y);
      }

      // context.moveTo(0, 0);
      // context.lineTo(380, 380);

      context.strokeStyle = '#aaa';
      context.stroke();
    });

    const nodes = (ref.current as HTMLDivElement).querySelectorAll('.box') as NodeListOf<HTMLDivElement>;

    let target: { x: 0; y: 0; element: any; active: boolean };

    function onUp(e: any) {
      const { clientX: x, clientY: y } = e;
      target = { x, y, element: e.target, active: false };
    }

    function onDown(e: any) {
      const { offsetTop: y, offsetLeft: x } = e.target;
      target = { x, y, element: e.target, active: true };
    }

    function onMove(e: any) {
      const { clientX: x, clientY: y } = e;

      if (target && target.active) {
        target.element.style.left = x - 100 + 'px';
        target.element.style.top = y - 100 + 'px';
      }
    }

    for (const node of Array.from(nodes)) {
      node.addEventListener('mouseup', onUp);
      node.addEventListener('mousedown', onDown);
      node.addEventListener('mousemove', onMove);
    }

    return () => {
      for (const node of Array.from(nodes)) {
        node.removeEventListener('mouseup', onUp);
        node.removeEventListener('mousedown', onDown);
        node.removeEventListener('mousemove', onMove);
      }
    };
  }, []);

  const NodesHTML = Object.entries(nodesData).map((node: any) => {
    const [id, data] = node;
    return (
      <div className="box" id={id} key={id} style={{ left: data.x, top: data.y }}>
        {data.name}
      </div>
    );
  });

  return (
    <div className={s.page} ref={ref}>
      <canvas ref={canvasRef} width={window.innerWidth} height={window.innerHeight}></canvas>
      {NodesHTML}
    </div>
  );
}
