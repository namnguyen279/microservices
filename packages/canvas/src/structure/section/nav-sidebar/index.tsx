import React, { RefObject, useEffect, useImperativeHandle, useRef, useState } from 'react';

import { Route } from 'core';
import { createRef, Ref, forwardRef } from 'react';
import s from './nav.module.scss';

function Home({ route, props }: any, pRef: Ref<any>) {
  const ref = useRef() as RefObject<HTMLOListElement>;

  useImperativeHandle(pRef, () => ({}));

  return (
    <div className={s.section}>
      <ol ref={ref} className="top">
        <li>Logo</li>
        <li>POST</li>
        <li>PAGE</li>
      </ol>
      <ol ref={ref} className="bottom">
        <li>USER</li>
        <li>GEAR</li>
      </ol>
    </div>
  );
}

export default forwardRef(Home);
