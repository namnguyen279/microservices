import React, { RefObject, useEffect, useImperativeHandle, useRef, useState } from 'react';

import { Route } from 'core';
import { createRef, Ref, forwardRef } from 'react';
import s from './header.module.scss';

function Home({ route, props }: any, pRef: Ref<any>) {
  const ref = useRef() as RefObject<HTMLOListElement>;

  useImperativeHandle(pRef, () => ({}));

  return (
    <ol className={s.section} ref={ref}>
      <li>Logo</li>
      <li>POST</li>
      <li>PAGE</li>
      <li>GEAR</li>
    </ol>
  );
}

export default forwardRef(Home);
