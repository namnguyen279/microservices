import React, { RefObject, useEffect, useImperativeHandle, useRef, useState } from 'react';

import { Route } from 'core';
import { createRef, Ref, forwardRef } from 'react';
import s from './header.module.scss';

function Home({ route, props }: any, pRef: Ref<any>) {
  const ref = useRef() as RefObject<HTMLDivElement>;

  useImperativeHandle(pRef, () => ({}));

  return <div>Body</div>;
}

export default forwardRef(Home);
