const path = require('path');

const cwd = process.cwd();

const Dotenv = require('dotenv-webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const publicPath = path.join(cwd, 'public');

const alias = {};

module.exports = (env) => {
  console.log('#############################');
  console.log('Mode:', 'Development');
  console.log('#############################');

  return {
    // context: cwd,
    mode: 'development',
    target: 'web',
    devtool: 'cheap-module-source-map',
    entry: {
      index: { import: path.resolve(cwd, 'src/index.tsx') },
    },
    output: {
      path: path.resolve(cwd, 'build'),
      filename: '[name].[fullhash].bundle.js',
      publicPath: '/',
    },
    plugins: [
      new Dotenv(),
      new HtmlWebpackPlugin({
        template: `${publicPath}/index.html`,
      }),
    ],
    resolve: {
      alias,
      extensions: ['.ts', '.tsx', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.(ts|tsx)?$/,
          use: ['babel-loader', 'ts-loader'],
          exclude: /node_modules/,
        },
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          use: { loader: 'babel-loader' },
        },
        {
          test: /\.md$/,
          exclude: /node_modules/,
          use: [{ loader: 'html-loader' }, { loader: 'markdown-loader', options: {} }],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: /\.module\.(sa|sc|c)ss$/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }],
        },
        {
          test: /\.module\.(sa|sc|c)ss$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader', options: { importLoaders: 1, modules: true } },
            { loader: 'sass-loader' },
          ],
          include: /\.module\.(sa|sc|c)ss$/,
        },
        {
          test: /\.svg$/,
          exclude: /node_modules/,
          loader: 'file-loader',
        },
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          exclude: /node_modules/,
          loader: 'file-loader',
        },

        {
          test: /\.json$/,
          loader: 'json-loader',
          type: 'javascript/auto',
        },
      ],
    },
    experiments: {
      topLevelAwait: true,
    },
    watchOptions: {
      ignored: '**/node_modules',
    },
    devServer: {
      port: 3000,
      historyApiFallback: true,
    },
  };
};
