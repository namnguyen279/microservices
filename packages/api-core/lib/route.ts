import { Router } from 'express';
import { RequestHandler, RequestProps } from './request';
import { Service } from './service';

export abstract class Route extends Service {
  public router: Router;
  public path: string = '/';
  public props!: RequestProps;
  private preFunc: Function[] = [];
  private postFunc: Function[] = [];

  constructor(routePath: string = '/', router?: Router) {
    super();
    this.path = routePath;
    this.router = router || Router();
  }

  public set Router(router: Router) {
    this.router = router;
  }

  public set Route(routePath: string) {
    this.path = routePath;
  }

  public preApply(...fns: Function[]) {
    this.preFunc = fns;
  }

  public postApply(...fns: Function[]) {
    this.postFunc = fns;
  }

  public get(path: string, ...fns: Function[]) {
    const mw = [...this.preFunc, ...fns, ...this.postFunc];
    const handler = new RequestHandler(this, mw, this.router);
    this.router.get(path, handler.intercept);
  }

  public post(path: string, ...fns: Function[]) {
    const handler = new RequestHandler(this, fns, this.router);
    this.router.post(path, handler.intercept);
  }

  public put(path: string, ...fns: Function[]) {
    const mw = [...this.preFunc, ...fns, ...this.postFunc];
    const handler = new RequestHandler(this, mw, this.router);
    this.router.put(path, handler.intercept);
  }

  public patch(path: string, ...fns: Function[]) {
    const mw = [...this.preFunc, ...fns, ...this.postFunc];
    const handler = new RequestHandler(this, mw, this.router);
    this.router.patch(path, handler.intercept);
  }

  public delete(path: string, ...fns: Function[]) {
    const mw = [...this.preFunc, ...fns, ...this.postFunc];
    const handler = new RequestHandler(this, mw, this.router);
    this.router.delete(path, handler.intercept);
  }
}
