import { Service } from './service';

export abstract class Plugin extends Service {
  abstract install(): void;
}
