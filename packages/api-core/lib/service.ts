import { Application } from './application';

export abstract class Service {
  public application!: Application;

  public set Application(app: Application) {
    this.application = app;
  }

  public abstract install(): void;
}
