class Logger {
  private errorCount: number = 0;
  error(...args: unknown[]) {
    this.errorCount++;
    console.error('\x1b[31m', '#🆘', `ERROR[#${this.errorCount}]:`, ...args, '\x1b[0m');
  }

  notValid(...args: unknown[]) {
    console.error('\x1b[35m', '#📛', `INVALID:`, ...args, '\x1b[0m');
  }

  ok(...args: unknown[]) {
    console.log('\x1b[32m', '#🆗', ...args, '\x1b[0m');
  }

  warn(...args: unknown[]) {
    console.warn('\x1b[33m', '#🔅', ...args, '\x1b[0m');
  }

  print(...args: unknown[]) {
    return console.log(...args);
  }
}

export const Log = new Logger();
