import express, { Express } from 'express';

import { Route } from './route';

import { Service } from './service';

import { Plugin } from './plugin';

import cookie from 'cookie-parser';

import glob from 'glob';

import path from 'path';

import fs from 'fs';

import { Log } from './util';

export enum ModuleType {
  Route = 'Route',
  Service = 'Service',
  Plugin = 'Plugin',
}

export type Options = {
  port: number;
  name: string;
  autoloadPaths: string[];
};

const optsDefault = {
  port: 2345,
  name: 'Không tên',
  autoloadPaths: ['plugins', 'services'],
};

export abstract class Application {
  public expressApp: Express = express();
  public appName: string;
  public appPort: number = 2345;
  public autoloadPaths: string[];

  public static projectPath = Application.detectProjectPath() as string;

  public get projectPath(): string {
    return Application.projectPath;
  }

  constructor(options: Partial<Options> = optsDefault) {
    const { port, name, autoloadPaths } = { ...optsDefault, ...options };

    this.appName = name;
    this.appPort = port;
    this.autoloadPaths = autoloadPaths;

    this.expressApp.use(express.urlencoded({ extended: false }));
    this.expressApp.use(express.json());
    this.expressApp.use(cookie());
  }

  public async start(options: Partial<Options> = optsDefault) {
    const { port, name, autoloadPaths } = { ...optsDefault, ...options };
    this.appName = name;
    this.appPort = port;
    this.autoloadPaths = autoloadPaths;

    try {
      await this.autoload(autoloadPaths);

      this.expressApp.listen(this.appPort, () => {
        // eslint-disable-next-line no-console
        Log.ok(`⚡️ App: "${this.appName}" đang mở cổng: ${port || this.appPort}`);
      });

      return this;
    } catch (e) {
      Log.error(e.name, e.message);
    }
  }

  // Install Module
  public async use(ExtendedModule: { new (): Route | Service | Plugin }): Promise<Route | Service | Plugin> {
    try {
      const proto = ExtendedModule.prototype;

      if (proto instanceof Route) {
        const route = new ExtendedModule() as Route;

        route.application = this;

        this.expressApp.use(route.path, route.router);

        await route.install();

        return route;
      }

      if (proto instanceof Plugin || proto instanceof Service) {
        const module = new ExtendedModule();

        module.application = this;

        // Log
        if (proto instanceof Plugin) {
          Log.ok('📦 Plugin', ExtendedModule.name, 'installed!');
        }

        await module.install();

        return module;
      }
    } catch (e) {
      Log.error(e.name, e.message);
      throw new Error(e);
    }

    Log.error(`class '${ExtendedModule.name}' cần extends từ các module ${Object.keys(ModuleType)}`);
    throw new Error(`class '${ExtendedModule.name}' cần extends từ các module ${Object.keys(ModuleType)}`);
  }

  private static detectProjectPath() {
    //singleton
    if (Application.projectPath) {
      return Application.projectPath;
    }

    //excludes junks
    const fullPaths = process.argv.filter((path) => {
      return path.includes(process.cwd());
    });

    const regex = /[^\\]*(js|ts)$/; // end with js or ts

    // filter more junk
    const folders = fullPaths.reduce((list: string[], next: string) => {
      if (next.match(regex) && next.includes(process.cwd())) {
        const paths = next.split('/').filter((path) => {
          return path.endsWith('.js') == false && path.endsWith('.ts') == false && path.length > 0;
        });

        list.push('/' + paths.join('/'));
      }

      return list;
    }, []);

    if (folders.length != 1) {
      throw new Error('No Path or More than 1 path is detect, make sure you run application from project folder!');
    }

    return folders[0];
  }

  private async autoload(autoloadPaths: string[]) {
    if (autoloadPaths && autoloadPaths?.length > 0) {
      Log.ok('📁 Autoload folders:', autoloadPaths.join(','));

      const promisePaths = autoloadPaths.map((path) => {
        return this.load(path);
      });

      return Promise.all(promisePaths);
    }
  }

  private async load(folderName: string) {
    try {
      const files = glob.sync(`${this.projectPath}/${folderName}/**/*.{ts,js}`);
      if (!fs.existsSync(`${this.projectPath}/${folderName}`)) {
        Log.warn(`[Bỏ Qua] Folder "${folderName}" không tồn tại`);
      }

      for (const fileName of files) {
        if (fileName.endsWith('.d.ts')) {
          continue;
        }

        const filePath = path.resolve(fileName);

        const file = require(filePath);

        if (!file.default) {
          Log.error(`/${folderName}/${fileName} không có export default`);
          continue;
        }

        await this.use(file.default);
      }
    } catch (e) {
      // No folder Found
      Log.error(e.name, 'cant load folder', folderName, 'path:', `${this.projectPath}/${folderName}`);
      if (process.env.NODE_ENV != 'production') {
        Log.print('-------------------------------');
        Log.error(e);
        Log.print('-------------------------------');
      }
    }
  }
}
