import { Router, Request, Response } from 'express';
import { HttpError } from './error';
import { Route } from './route';

import { Log } from './util';

export type RequestProps = {
  params: Record<string | number, unknown>;
  cookies: Record<string | number, unknown>;
  query: Record<string | number, unknown>;
  headers: Record<string | number, unknown>;
  body: Record<string | number, unknown>;
  req: Request;
  res: Response;
};

export class RequestHandler {
  router: Router;
  fns: Function[];
  req!: Request;
  res!: Response;
  origin: Route;

  constructor(context: Route, fns: Function[], router: Router) {
    this.router = router;
    this.origin = context;
    this.fns = fns;
    this.intercept = this.intercept.bind(this);
  }

  async *fn(props: any): any {
    let result;
    for (const index in this.fns) {
      result = await this.fns[index](result, props);
      yield result;
    }
    return;
  }

  public async intercept(req: Request, res: Response) {
    this.req = req;
    this.res = res;

    const requestProps = {
      params: this.req.params,
      cookies: this.req.cookies,
      signedCookies: this.req.signedCookies,
      query: this.req.query,
      headers: this.req.headers,
      body: this.req.body,
      req,
      res,
    } as RequestProps;

    try {
      if (this.fns instanceof Array) {
        const origin = Object.assign({ props: requestProps }, this.origin); // extends context
        this.fns = this.fns.map((e) => e.bind(origin));
        const nx = this.fn(requestProps);

        let result;
        for await (const index of this.fns) {
          const fn = await nx.next();
          result = fn.value;
        }
        return res.json(result);
      }

      throw new HttpError(500, 'Unknown Error');
    } catch (e) {
      if (e.code && e.code == 500) {
        Log.error(e);
      }

      if (process.env.NODE_ENV != 'production') {
        Log.print('-------------------------------');
        Log.error(e);
        Log.print('-------------------------------');
      }

      if (e.code) {
        this.res?.status(e.code).json({ error: true, code: e.code, message: e.message });
        return;
      }

      this.res?.status(500).json({ error: true, code: 500, message: e.message });
    }
  }
}
