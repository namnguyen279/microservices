export class ExpressError extends Error {
  code: number;
  constructor(code: number, message: string) {
    super(message);
    this.message = message;
    this.code = code;
    this.name = 'ExpressError';
  }
}

export class HttpError extends Error {
  code: number = 500;
  constructor(code: number, message: string) {
    super(message);
    this.message = message;
    this.code = code;
    this.name = 'HttpError';
  }
}
