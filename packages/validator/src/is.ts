import ValidationError from './error';

function IsEmail(value: string, error?: ValidationError): ValidationError {
  return error || new ValidationError(100, '$email is invalid Email', { email: value });
}

function IsNumber(value: string, error?: ValidationError): ValidationError {
  return error || new ValidationError(200, '$number is invalid Numer', { number: value });
}

export default {
  IsEmail,
  IsNumber,
};
