import ValidationError from './error';
import { JsonModel, Schema, ValidateOption } from '.';

export function get(obj: any, path: any, defaultValue: any = undefined) {
  const travel = (regexp: any) =>
    String.prototype.split
      .call(path, regexp)
      .filter(Boolean)
      .reduce((res: any, key: any) => (res !== null && res !== undefined ? res[key] : res), obj);
  const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
  return result === undefined || result === obj ? defaultValue : result;
}

export function set(obj: any, path: any, value: any) {
  if (Object(obj) !== obj) return obj; // When obj is not an object
  if (!Array.isArray(path)) path = path.toString().match(/[^.[\]]+/g) || [];
  path
    .slice(0, -1)
    .reduce(
      (a: any, c: any, i: any) =>
        Object(a[c]) === a[c] ? a[c] : (a[c] = Math.abs(path[i + 1]) >> 0 === +path[i + 1] ? [] : {}),
      obj,
    )[path[path.length - 1]] = value;
  return obj;
}

export type Premitive = string | boolean | number | Date | Object;

export interface DataOption {
  strict: boolean;
}

export class DataObject {
  private _schema: Schema;
  private _errors: ValidationError[] = [];
  private _options: DataOption;
  private _data = {} as Record<string, Premitive>;

  constructor(schema: Schema, data: Record<string, Premitive>, options: DataOption) {
    this._schema = schema;
    this._options = options;

    for (const name in data) {
      const field = this._schema.model[name];
      this.deepInsert(name, field, null, data[name]);
    }
  }

  private deepInsert(name: string, field: JsonModel | ValidateOption, parent: string | null = null, data: Premitive) {
    const key = `${parent ? parent + '.' : ''}${name}`; // nested.nested.value

    // SET PRIMITIVE KEY VALUE
    if (field && field.type && typeof data != 'object') {
      set(this._data, key, data);
    }

    // SET NESTED KEY VALUE
    if (field && !field.type && typeof field == 'object' && typeof data == 'object') {
      for (const subName in data) {
        const subField = (field as JsonModel)[subName];

        if (subField && subField.type) {
          this.deepInsert(subName, subField, key, (data as Object)[subName as keyof Object]);
          continue;
        }

        // WITH STRICT OPTION, throw Error if data provided mis-matched with schema
        if (this._options.strict) {
          const fullPath = `${key ? key + '.' : ''}${subName}`; // nested.nested.value
          this.pushError = new ValidationError(1, '$attr mis-matched with schema', {
            attr: fullPath,
          });
        }
      }
    }
  }

  private deepValidate(name: string, field: JsonModel | ValidateOption, parent = '') {
    const key = `${parent ? parent + '.' : ''}${name}`; // nested.nested.value

    if (field && field.type) {
      const { type, required, is } = field as { is: string[]; required: boolean; type: string | Function };
      const attrType = typeof type == 'string' ? type : (type as Function).name; // Stringify to "Number" | "String"
      let value = get(this._data, key, null);
      value = typeof value == 'string' && value.trim().length == 0 ? null : value;

      if (value != null && value.constructor.name !== attrType) {
        this.pushError = new ValidationError(2, '$attr should be type $type', { attr: name, type: attrType });
      }

      if (required == true) {
        if (value == null || (typeof value == 'string' && (value.trim() as string).length == 0)) {
          this.pushError = new ValidationError(2, '$attr is required but found empty', {
            attr: key,
          });
        }
      }

      if (value == null && typeof required == 'string') {
        const model = get(this._schema.model, required, null);
        let refValue = get(this._data, required, null);
        refValue = typeof refValue == 'string' && refValue.trim().length == 0 ? null : refValue;
        if (model == null) {
          this.pushError = new ValidationError(2, '$attr schema is not found', {
            attr: required,
          });
        }

        if (model && refValue != null && value == null) {
          this.pushError = new ValidationError(2, '$attr will required when $required is available but found empty', {
            attr: key,
            required,
          });
        }
      }

      // Custom Validator
      if (is && is.length > 0 && value != null) {
        const validators: any = Schema.validators;

        for (const fn of is) {
          // STRING validator
          if (typeof fn == 'string' && validators[fn]) {
            const error = validators[fn](value);
            this.pushError = error;
            continue;
          }

          // Custom Validator Error Object
          if (typeof fn == 'object' && validators[(fn as any).type]) {
            const { type, message, code, properties } = fn as any;
            const error = validators[type as keyof Object](value, new ValidationError(code, message, properties));
            this.pushError = error;
            continue;
          }

          if (this._options.strict) {
            const fnName = typeof fn == 'string' ? fn : (fn as any).type;
            this.pushError = new ValidationError(4, '$fnName is not a valid Validator', { fnName });
          }
        }
      }
    }

    // Nested Object should be object with No type field
    if (field && !field.type && typeof field == 'object') {
      for (const subName in field) {
        const subField = (field as JsonModel)[subName];
        this.deepValidate(subName, subField, key);
      }
    }
  }

  public validate() {
    const model = this._schema.model;
    for (const name in model) {
      const field = this._schema.model[name];
      this.deepValidate(name, field);
    }
  }

  public set pushError(error: ValidationError) {
    this._errors.push(error);
  }

  public get errors() {
    return this._errors;
  }

  public get errorsArray() {
    return this._errors.map((e) => e.error);
  }
}
