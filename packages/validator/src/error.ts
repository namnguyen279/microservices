export default class ValidationError {
  code: number;
  message?: string;
  properties?: Record<string, string | number>;

  constructor(code: number, message?: string, properties?: Record<string, string>) {
    this.code = code;
    this.message = message;
    this.properties = properties;
  }

  get error() {
    let { code, message } = this;

    if (this.properties) {
      Object.entries(this.properties).forEach((property) => {
        const [attrName, replacement] = property;
        message = message?.replace(`$${attrName}`, `${replacement}`);
      });
    }

    return { code, message };
  }
}
