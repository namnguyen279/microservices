import { DataObject, Premitive } from './data';
import ValidationError from './error';
import validators from './is';

export type ValidateOption = {
  type: typeof String | typeof Number | typeof Object | string;
  description?: string;
  required?: boolean | string;
  is?: ({ type: string; code: number; message: string; properties?: Object } | string)[];
};

export interface JsonModel {
  [name: string]: ValidateOption | JsonModel;
}

export class Schema {
  private json: JsonModel;
  static validators = validators;

  constructor(model: JsonModel) {
    this.json = model;
  }

  public get model() {
    return this.json;
  }

  set(data: Record<string, Premitive>, options = { strict: true }): DataObject {
    return new DataObject(this, data, options);
  }

  public static registerValidator(fn: (p: { value: string; error: ValidationError }) => ValidationError | undefined) {
    const name = (fn as any).name;
    if (!fn.name || (Schema.validators as any)[name] != null) {
      return console.error('Validator require an Unique Name');
    }

    (Schema.validators as any)[name] = fn;
  }

  public toJson() {
    try {
      return JSON.stringify(this.json);
    } catch (e) {
      console.error('# Fail to Stringify Json Data');
    }
  }

  static fromJson(json: string) {
    try {
      const model = JSON.parse(json);
      return new Schema(model);
    } catch (e) {
      console.error('# Fail to load Json Data');
    }
  }
}
