import { Schema } from '../src';

const schema = new Schema({
  text: { type: 'Number', required: true },
  email: { type: String, is: ['IsEmail'] },
  nested: {
    name: { key: { type: String, is: [{ type: 'IsNumber', code: 200, message: 'Number Fail' }] } },
    some: { type: String, required: 'nested.name.key' },
  },
});

const modelWithData = schema.set({
  text: '2',
  email: 'a@sa.com',
  nested: { name: 'sss' },
});

console.log(modelWithData.validate());
console.log(modelWithData.errorsArray);

// console.log(require('util').inspect(a, false, null, true /* enable colors */));
