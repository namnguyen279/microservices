import { Application } from '@vcsc/node-core';

class App extends Application {
  constructor() {
    super();
    this.start({ name: 'Test-App', autoloadPaths: ['plugins', 'services'], port: 3000 });
  }
}

new App();
