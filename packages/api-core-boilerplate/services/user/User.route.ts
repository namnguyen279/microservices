import { Route, RequestProps } from '@vcsc/node-core';
import { HttpError } from '@vcsc/node-core/lib/error';
import UserService from './User.service';
import Socket from 'plugins/socket';

export default class UserRoute extends Route {
  userService = UserService.Instance;

  install() {
    this.preApply(this.auth);
    this.get('/', this.validate, this.business);
    this.get('/2', this.validate, this.business);
  }

  auth() {
    return 'ss';
  }

  async validate(props: RequestProps) {
    console.log(Socket);
    // console.log(this);
    // throw new HttpError(403, 'Something');
    return 'aaa';
  }

  async business(result: string, props: RequestProps) {
    // props.throw403();
    // console.log(this);
    return result + '2222';
    // const token = await this.userService.login();
    // return token + '.second';
  }

  format(result: string) {
    return result + '.after format';
  }
}
