import { Service } from '@vcsc/node-core';
import { Log } from '@vcsc/node-core';

export default class UserService extends Service {
  private static readonly instance: UserService = new UserService();

  public static get Instance(): UserService {
    return UserService.instance;
  }

  public install(): void {}

  say() {
    Log.error('something', 200);
  }

  async login() {
    return 'login.success';
  }
}
