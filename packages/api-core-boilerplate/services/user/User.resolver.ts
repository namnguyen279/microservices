import { GraphRequestProps, Resolver } from '@plugins/graphql';
import UserService from './User.service';

export default class UserResolver extends Resolver {
  userService = UserService.Instance;

  public install(): void {
    this.query('abc', this.abc, this.ccc);
  }

  async abc(props: GraphRequestProps) {
    return 'first';
  }

  async ccc(result: string, props: GraphRequestProps) {
    return result + 'second';
  }
}
