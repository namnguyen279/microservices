const { io } = require('socket.io-client');

const socket = io('ws://localhost:3005', { transports: ['websocket'] });

socket.on('connect', () => {
  console.log(`reconnected --- ${socket.connected}`); // true
  socket.emit('test', 'hello server');
});

socket.on('disconnect', () => {
  console.log(`disconnected --- ${socket.connected}`); // false
});

socket.on('connect_error', () => {
  //socket.auth.token = "abcd";
  console.log('connect_error');
  socket.connect();
});

socket.on('match-price-HAG', (message) => {
  console.log(message);
});
