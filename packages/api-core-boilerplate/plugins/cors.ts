import { Plugin } from '@vcsc/node-core';
import cors from 'cors';

export default class Cors extends Plugin {
  install() {
    this.application.expressApp.use(cors());
  }
}
