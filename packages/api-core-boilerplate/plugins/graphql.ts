import { Plugin, Log, Service } from '@vcsc/node-core';
import { ApolloServer } from 'apollo-server-express';

import { makeExecutableSchema } from '@graphql-tools/schema';
import { GraphQLFileLoader } from '@graphql-tools/graphql-file-loader';
import { loadSchemaSync } from '@graphql-tools/load';

import { join } from 'path';

import { GraphQLSchema, GraphQLResolveInfo } from 'graphql';

export default class Graphql extends Plugin {
  static resolvers = {} as any;
  async install() {
    const schema = await this.loadSchema();
    return await this.hook(schema);
  }

  async loadSchema() {
    const graphqlPath = this.application.projectPath as string;

    const typeDefs = loadSchemaSync(join(graphqlPath, '/schema/*.gql'), {
      loaders: [new GraphQLFileLoader()],
    });

    const schema = makeExecutableSchema({
      typeDefs,
      resolvers: Graphql.resolvers,
    });

    return schema;
  }

  async hook(schema: GraphQLSchema) {
    const server = new ApolloServer({
      schema,
      formatError(e: Error) {
        Log.notValid(e.name, e.message);
        return e;
      },
    });

    await server.start();

    return server.applyMiddleware({ app: this.application?.expressApp as any });
  }
}

export type GraphRequestProps = {
  parent: null | undefined;
  args: any;
  context: any;
  info?: GraphQLResolveInfo;
};

export abstract class Resolver extends Service {
  async query(name: string, ...fns: Function[]) {
    if (!Graphql.resolvers.Query) {
      Graphql.resolvers.Query = {};
    }

    if (Graphql.resolvers.Query[name]) {
      Log.warn('Query tên', name, 'đã tồn tại');
    }

    Graphql.resolvers.Query[name] = async function (parent: any, args: any, context: any, info: GraphQLResolveInfo) {
      const requestProps: Partial<GraphRequestProps> = { parent, args, context, info };

      if (fns instanceof Array) {
        function reducer(chain: any, func: any) {
          return chain.then(func, requestProps);
        }
        // Chạy danh sách function từ trái qua phải
        const result = await fns.reduce(reducer, Promise.resolve(requestProps));

        return result;
      }

      return null;
    };
  }

  mutation(name: string, ...fns: Function[]) {
    if (!Graphql.resolvers.Mutation) {
      Graphql.resolvers.Mutation = {};
    }

    if (Graphql.resolvers.Mutation[name]) {
      Log.warn('Mutation tên', name, 'đã tồn tại');
    }

    Graphql.resolvers.Mutation[name] = async function (parent: any, args: any, context: any, info: GraphQLResolveInfo) {
      const requestProps: Partial<GraphRequestProps> = { parent, args, context, info };

      if (fns instanceof Array) {
        function reducer(chain: any, func: any) {
          return chain.then(func, requestProps);
        }
        // Chạy danh sách function từ trái qua phải
        const result = await fns.reduce(reducer, Promise.resolve(requestProps));

        return result;
      }

      return null;
    };
  }
}
