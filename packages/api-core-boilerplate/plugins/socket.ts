import { Plugin } from '@vcsc/node-core';
import http from 'http';
import { Server, Socket } from 'socket.io';

export default class SocketApp extends Plugin {
  public io!: Server;

  install() {
    // this.application.expressApp.use(cors());
    const app = this.application.expressApp;

    const server = http.createServer(app);

    this._io = new Server(server);
  }

  start() {
    this.io.on('connection', (socket) => {
      console.log('a user connected');
    });
  }
}
