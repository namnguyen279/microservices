import React from 'react';
import logo from './logo.svg';
import './App.css';
import { KcApp, defaultKcProps, getKcContext, useKcLanguageTag } from 'keycloakify';

const { kcContext } = getKcContext();

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <form action={kcContext?.url.loginAction} method="post">
          <div className="form-group">
            <label htmlFor="username" className="pf-c-form__label pf-c-form__label-text">
              Username or email
            </label>

            <input tabIndex={1} id="username" name="username" type="text" autoFocus={true} autoComplete="off" />
          </div>

          <div className="form-group">
            <label htmlFor="password" className="pf-c-form__label pf-c-form__label-text">
              Password
            </label>

            <input tabIndex={2} id="password" name="password" type="password" autoComplete="off" />
          </div>

          <div className="form-group login-pf-settings">
            <div id="kc-form-options"></div>
            <div className=""></div>
          </div>

          <div id="kc-form-buttons" className="form-group">
            <input type="hidden" id="id-hidden-input" name="credentialId" />
            <input tabIndex={4} name="login" id="kc-login" type="submit" value="Sign In" />
          </div>
        </form>
      </header>
    </div>
  );
}

export default App;
