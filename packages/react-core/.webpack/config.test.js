const path = require('path');

const cwd = process.cwd();

const fs = require('fs');

const alias = {};

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const glob = require('glob');

const cssModuleMap = (pattern) => {
  return glob.sync(pattern).reduce((entries, filename) => {
    const [, name] = filename.match(/([^/]+)\.module.scss$/);
    return { ...entries, ['css/' + name]: filename };
  }, {});
};

const indexModuleMap = (pattern) => {
  return glob.sync(pattern).reduce((entries, filename) => {
    const [, name] = filename.match(/([^/]+).index.tsx$/);
    return { ...entries, [name]: filename };
  }, {});
};

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function (file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

const entry = {};

const components = getDirectories('./lib');

for (var i = 0; i < components.length; i++) {
  entry[components[i]] = './lib/' + components[i] + '/' + components[i] + '.tsx';
}

console.log(entry);

module.exports = (env) => {
  console.log('#############################');
  console.log('Mode:', 'Development');
  console.log('#############################');
  // console.log(...indexModuleMap(cwd + '/lib/**/index.tsx'));
  return {
    context: cwd,
    mode: 'development',
    target: 'web',
    devtool: 'cheap-module-source-map',
    entry: {
      'ui.button': './lib/Button/index.tsx',
      // ...indexModuleMap(cwd + '/lib/**/index.tsx'),
    },
    output: {
      path: path.resolve(cwd, 'dist'),
      filename: '[name].js',
      library: '@vcsc/react-core',
      libraryTarget: 'umd',
    },
    plugins: [new MiniCssExtractPlugin()],
    resolve: {
      alias,
      extensions: ['.ts', '.tsx', '.js', '.scss'],
    },
    module: {
      rules: [
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          use: { loader: 'babel-loader' },
        },
        {
          test: /\.md$/,
          exclude: /node_modules/,
          use: [{ loader: 'html-loader' }, { loader: 'markdown-loader', options: {} }],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          exclude: /\.module\.(sa|sc|c)ss$/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }],
        },
        {
          test: /\.module\.(sa|sc|c)ss$/,
          include: /\.module\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { importLoaders: 1, modules: true } },
            { loader: 'sass-loader' },
          ],
        },
        {
          test: /\.svg$/,
          exclude: /node_modules/,
          loader: 'file-loader',
        },
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          exclude: /node_modules/,
          loader: 'file-loader',
        },
        {
          test: /\.(ts|tsx)?$/,
          use: ['babel-loader', 'ts-loader'],
          exclude: /node_modules/,
        },
        {
          test: /\.json$/,
          loader: 'json-loader',
          type: 'javascript/auto',
        },
      ],
    },
    experiments: {
      topLevelAwait: true,
    },
    watchOptions: {
      ignored: '**/node_modules',
    },
  };
};
