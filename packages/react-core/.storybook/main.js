module.exports = {
  core: {
    builder: 'webpack5',
  },
  stories: ['../lib/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  framework: '@storybook/react',
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push({
      test: /\.(sa|sc|c)ss$/,
      exclude: /\.module\.(sa|sc|c)ss$/,
      use: [{ loader: 'style-loader' }, { loader: 'css-loader' }, { loader: 'sass-loader' }],
    });

    config.module.rules.push({
      test: /\.module\.(sa|sc|c)ss$/,
      use: [
        { loader: 'style-loader', options: {} },
        { loader: 'css-loader', options: { importLoaders: 1, modules: true } },
        { loader: 'sass-loader', options: {} },
      ],
      include: /\.module\.(sa|sc|c)ss$/,
    });

    // config.module.rules.push({
    //   test: /\.module\.(sa|sc|c)ss$/,
    //   use: [
    //     { loader: require('styled-jsx/webpack').loader, options: { type: 'scoped' } },
    //     { loader: 'css-loader', options: { importLoaders: 1, modules: true } },
    //     { loader: 'sass-loader', options: {} },
    //   ],
    //   include: /\.module\.(sa|sc|c)ss$/,
    // });

    return config;
  },
  // typescript: {
  //   check: false,
  //   checkOptions: {},
  //   reactDocgen: 'react-docgen-typescript',
  //   reactDocgenTypescriptOptions: {
  //     shouldExtractLiteralValuesFromEnum: true,
  //     propFilter: (prop) => (prop.parent ? !/node_modules/.test(prop.parent.fileName) : true),
  //   },
  // },
};
