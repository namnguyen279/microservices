// import _JSXStyle from 'styled-jsx/style'

import React from 'react';
// import cl from 'classnames';

// import $ from './button.module.scss';
// import css from 'styled-jsx/css';
import s from './button.style';
// console.log(typeof style);
// const A = css``;

// console.log(A);
// console.log($);
// const button = css`
//   button {
//     color: hotpink;
//   }
// `;

export interface ButtonProps {
  size: 'small' | 'medium' | 'large';
  primary: boolean;
  label: string;
}

export const Button = function ({ label, ...props }: Partial<ButtonProps>) {
  console.log(s);
  return <button {...props}>{label}</button>;
};
