export interface ButtonProps {
    size: 'small' | 'medium' | 'large';
    primary: boolean;
    label: string;
}
export declare const Button: ({ label, ...props }: Partial<ButtonProps>) => JSX.Element;
