# Directories

Project yêu cầu tuân thủ việc sử dụng standard folder naming convention đưới đây.

**Note: trường hợp việc phân mục của application bạn làm không hợp lý khi sử dụng các folder dưới đây, vui lòng raise với team**

## Project Structure

Sử dụng các tên folder dưới đây với các mục đích như sau:

1. **Controllers**: /controllers/ -- Các root controller, sử dụng để lưu controller cho endpoint
2. **Models**: /models/ -- Lưu raw db model từ ORM, SQL, etc, raw table từ db
3. **Modules**: /modules/ -- Các modules khác
4. **Schema**: /schemas/ -- Schema, Graphql Schema
5. **Services**: /services/ -- Business / Process Logic / Custom Logic
6. **Views**: /views/ -- views for view engine
7. **Public**: /public/ -- public assets (png/pdf/jpg/json)
8. **Private**: /private/ -- private assets
9. **Temporary**: /tmp/ -- temporary, use for tempory cache, generated contents,
10. **Worker**: /workers/ -- contain worker script, cronjob script
11. **Migration**: /migrations/ -- db migrations
12. **Bin**: /bin/ -- executable script (.sh, etc)
13. **Build**: /build/ -- build js version
14. **Log**: /logs/ -- logs if has
15. **Config**: /configs/ -- constant, configs if have (for constant please use Enum)
16. **@Types**: /@types/ -- autoload type/interface
17. **Packages**: /packages/ -- some imported code, khả năng không biết đến từ đâu
18. **decorators**: /decorators/ -- Custom decorator functions để ở đây
19. **Lib** /lib/

### Nested module pattern

- Đảm bảo tính đóng gói các Application logic thông qua việc tạo folder cho từng module:
  - Ví dụ `/services/serviceName` và có nested strucures như `@types` và `decorators`

**Nested Structures:**

```
    - /services/user
    - /services/user/@types
    - /services/user/decorators
```

## Front-End

Trên front-end, 2 folders dưới đây được thêm vào danh sách

1. Components: /components/ -- shared components
2. Page: /pages/ -- router theo trang (sử dụng thay cho /controllers/)

# Files

### 1. Index.ts tại root - Yêu cầu 1 file index.ts để boot dự án ở root

### 2. Tách môi trường development và production

# Code structure standard

### 1. Mỗi Module hoặc Service sẽ có 1 folder riêng, trừ các case có file duy nhất.

```
    // Service User gồm 1 file index "services/user/index.ts" chứa class như sau
    export default class User {

    }

```

### 2. Không trực tiếp import từ trong các module(folder) hoặc nested module. Các function/class phải expose ra ngoài cần declare export trong file index.ts

**KHÔNG NÊN**

```
   // Không import trực tiếp module từ file bên trong module
   import someController from 'services/users/someController.ts'
   import someUtil from 'serviecs/users/util/someUtil.ts'
```

**NÊN**

```
    // Export thông qua file index.ts để highlight các chức năng hỗ trợ
    export someController from 'services/users/someController.ts' (trong "services/users/index.ts")
    export someUtil from 'services/users/someUtil.ts' (trong "serviecs/users/util/index.ts")

    // Import các file từ index only
    import someController from 'services/users'
    import someUtil from 'services/users/util'
```

### 3. Các shared type, interface đặt trong folder @types

### 4. Sử dụng class và static properties cho singleton, hạn chế sử dụng functional ngoại trừ các function dùng kết nối với lib khác

```
    // services/users/index.ts

    class User {
        static singleton = "something only declare once or has one instance";

        authenticate() {
            // Do something with sharing singleton;
            User.singleton();
        }
    }
```
